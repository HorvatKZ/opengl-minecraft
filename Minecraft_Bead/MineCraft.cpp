#include "MineCraft.h"

#include <math.h>
#include <vector>
#include <chrono>

#include <array>
#include <list>
#include <tuple>
#include <imgui/imgui.h>
#include "includes/GLUtils.hpp"



MineCraft::MineCraft(void)
{
	m_camera.SetView(glm::vec3(8, 100, 8), glm::vec3(8, 0, 0), glm::vec3(0, 1, 0));
}


MineCraft::~MineCraft(void)
{
	std::cout << "dtor!\n";
}


void MineCraft::InitChunks()
{
	// Initializing
	chunks.assign((2 * RENDER_DISTANCE - 1) * (2 * RENDER_DISTANCE - 1), nullptr);
	int center = RENDER_DISTANCE - 1;
	chunks[_center()] = new Chunk(glm::vec3(0, 0, 0));

	// Creating generation tasks
	for (int d = 1; d < RENDER_DISTANCE; ++d) {
		int i = center - d;
		int j = center - d;
		for (; i < center + d; ++i) {
			generationTasks.push({ glm::vec3((j - center) * 16, 0, (i - center) * 16) });
		}
		for (; j < center + d; ++j) {
			generationTasks.push({ glm::vec3((j - center) * 16, 0, (i - center) * 16) });
		}
		for (; i > center - d; --i) {
			generationTasks.push({ glm::vec3((j - center) * 16, 0, (i - center) * 16) });
		}
		for (; j > center - d; --j) {
			generationTasks.push({ glm::vec3((j - center) * 16, 0, (i - center) * 16) });
		}
	}

	// Create generation thread
	genThread = std::thread([this] { this->Generation(); });
}


void MineCraft::InitSkyBox()
{
	m_SkyboxPos.BufferData(
		std::vector<glm::vec3>{
		// back
		glm::vec3(-1, -1, -1),
		glm::vec3(1, -1, -1),
		glm::vec3(1, 1, -1),
		glm::vec3(-1, 1, -1),
		// front
		glm::vec3(-1, -1, 1),
		glm::vec3(1, -1, 1),
		glm::vec3(1, 1, 1),
		glm::vec3(-1, 1, 1),
	}
	);

	m_SkyboxIndices.BufferData(
		std::vector<int>{
			// back
			0, 1, 2,
			2, 3, 0,
			// front
			4, 6, 5,
			6, 4, 7,
			// left
			0, 3, 4,
			4, 3, 7,
			// right
			1, 5, 2,
			5, 6, 2,
			// bottom
			1, 0, 4,
			1, 4, 5,
			// top
			3, 2, 6,
			3, 6, 7,
	}
	);

	m_SkyboxVao.Init(
		{
			{ CreateAttribute<0, glm::vec3, 0, sizeof(glm::vec3)>, m_SkyboxPos },
		}, m_SkyboxIndices
	);

	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
	glGenTextures(1, &m_skyboxTexture);
	glBindTexture(GL_TEXTURE_CUBE_MAP, m_skyboxTexture);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	TextureFromFileAttach("assets/xpos.png", GL_TEXTURE_CUBE_MAP_POSITIVE_X);
	TextureFromFileAttach("assets/xneg.png", GL_TEXTURE_CUBE_MAP_NEGATIVE_X);
	TextureFromFileAttach("assets/ypos.png", GL_TEXTURE_CUBE_MAP_POSITIVE_Y);
	TextureFromFileAttach("assets/yneg.png", GL_TEXTURE_CUBE_MAP_NEGATIVE_Y);
	TextureFromFileAttach("assets/zpos.png", GL_TEXTURE_CUBE_MAP_POSITIVE_Z);
	TextureFromFileAttach("assets/zneg.png", GL_TEXTURE_CUBE_MAP_NEGATIVE_Z);

	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}


void MineCraft::InitShaders()
{
	m_program.Init(
		{
			{ GL_VERTEX_SHADER, "MineCraft.vert"},
			{ GL_FRAGMENT_SHADER, "MineCraft.frag"}
		},
		{
			{ 0, "vs_in_pos" },	
			{ 1, "vs_in_texx" },
			{ 2, "vs_in_texy" },
			{ 3, "vs_in_norm" },
		}
	);

	m_programSkybox.Init(
		{
			{ GL_VERTEX_SHADER, "skybox.vert" },
			{ GL_FRAGMENT_SHADER, "skybox.frag" }
		},
		{
			{ 0, "vs_in_pos" },
		}
	);

	m_programCrosshair.Init(
		{
			{ GL_VERTEX_SHADER, "crosshair.vert" },
			{ GL_FRAGMENT_SHADER, "crosshair.frag" }
		},
		{
			{ 0, "vs_in_pos" },
		}
	);

}


bool MineCraft::Init()
{
	glClearColor(0.125f, 0.25f, 0.5f, 1.0f);
	SDL_GL_SetSwapInterval(0);	// V-sync OFF

	GLint m_viewport[4];
	glGetIntegerv(GL_VIEWPORT, m_viewport);
	viewport_w = m_viewport[2];
	viewport_h = m_viewport[3];

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_LINE_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);

	InitShaders();
	InitChunks();
	InitSkyBox();

	m_atlasTexture.FromFile("assets/atlas.png");
	m_program.SetTexture("atlas", 0, m_atlasTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
	
	SDL_SetRelativeMouseMode(cursorInvisible ? SDL_TRUE : SDL_FALSE);
	m_camera.SetProj(45.0f, 640.0f / 480.0f, 0.01f, 1000.0f);

	return true;
}


void MineCraft::Clean()
{
	isEnded = true;
	genThread.join();

	glDeleteTextures(1, &m_skyboxTexture);

	for (int i = 0; i < chunks.size(); ++i) {
		if (chunks[i] != nullptr) {
			delete chunks[i];
		}
	}
}


void MineCraft::Update()
{
	static Uint32 last_time = SDL_GetTicks();
	float delta_time = (SDL_GetTicks() - last_time) / 1000.0f;

	m_camera.Update(delta_time);
	CountFPS(delta_time);

	last_time = SDL_GetTicks();

	// Check if new chunks need to be generated
	glm::vec3 pos = m_camera.GetEye(), center = glm::vec3(8 + sh * 16, 0, 8 + sv * 16);
	if (abs(pos.x - center.x) > 9.f || abs(pos.z - center.z) > 9.f) {
		ManageGenTasks(pos, center);
	}
	
}


void MineCraft::Render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::mat4 viewProj = m_camera.GetViewProj();
	glm::mat4 world = glm::mat4(1.0f);
	m_program.Use();
	m_program.SetUniform("MVP", viewProj * world);
	m_program.SetUniform("world", world);
	m_program.SetUniform("worldIT", glm::inverse(glm::transpose(world)));


	glm::vec2 pos = glm::vec2(m_camera.GetEye().x, m_camera.GetEye().z);
	lockChunks.lock();
	int maxRenderDistanceInBlocks = RENDER_DISTANCE * 16 - 8;
	for (int i = 0; i < chunks.size(); ++i) {
		if (chunks[i] != nullptr && distance(chunks[i]->GetCenterPos(), pos) < maxRenderDistanceInBlocks && isInView(chunks[i]->GetCenterPos())) {
			chunks[i]->Draw();
		}
	}
	lockChunks.unlock();

	m_program.Unuse();

	// skybox
	GLint prevDepthFnc;
	glGetIntegerv(GL_DEPTH_FUNC, &prevDepthFnc);
	glDepthFunc(GL_LEQUAL);

	m_SkyboxVao.Bind();
	m_programSkybox.Use();
	m_programSkybox.SetUniform("MVP", viewProj * glm::translate( m_camera.GetEye()) );
	
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, m_skyboxTexture);
	glUniform1i(m_programSkybox.GetLocation("skyboxTexture"), 0);

	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, nullptr);
	m_programSkybox.Unuse();

	glDepthFunc(prevDepthFnc);


	m_program.Use();
	lockChunks.lock();
	for (int i = 0; i < chunks.size(); ++i) {
		if (chunks[i] != nullptr && distance(chunks[i]->GetCenterPos(), pos) < maxRenderDistanceInBlocks && isInView(chunks[i]->GetCenterPos())) {
			chunks[i]->DrawTransparent();
		}
	}
	lockChunks.unlock();
	m_program.Unuse();

	m_programCrosshair.Use();

	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex2f(0.f, 20.0f / viewport_h);
	glVertex2f(0.f, -20.0f / viewport_h);
	glVertex2f(20.0f / viewport_w, 0.f);
	glVertex2f(-20.0f / viewport_w, 0.f);
	glEnd();
	m_programCrosshair.Unuse();
}


void MineCraft::KeyboardDown(SDL_KeyboardEvent& key)
{
	if (key.keysym.sym == SDLK_q) {
		cursorInvisible = !cursorInvisible;
		SDL_SetRelativeMouseMode(cursorInvisible ? SDL_TRUE : SDL_FALSE);
	}
	m_camera.KeyboardDown(key);
}


void MineCraft::KeyboardUp(SDL_KeyboardEvent& key)
{
	m_camera.KeyboardUp(key);
}


void MineCraft::MouseMove(SDL_MouseMotionEvent& mouse)
{
	m_camera.MouseMove(mouse);
}


void MineCraft::MouseDown(SDL_MouseButtonEvent& mouse)
{
	if (mouse.button == SDL_BUTTON_LEFT || mouse.button == SDL_BUTTON_RIGHT || mouse.button == SDL_BUTTON_MIDDLE) {
		int centerPosI = RENDER_DISTANCE - 1;
		int centerPosJ = RENDER_DISTANCE - 1;
		// center, up, down, left, right, up-left, up-right, down-left, down-right
		int diffI[] = { 0, -1, 1, 0, 0, -1, -1, 1, 1 };
		int diffJ[] = { 0, 0, 0, -1, 1, 1, -1, 1, -1 };
		Raycaster rtx(m_camera.GetEye(), m_camera.GetAt());
		lockChunks.lock();
		int i = 0;
		// Run raycaster till it hits a block
		while (i < 9 && (chunks[_indexer(centerPosI + diffI[i], centerPosJ + diffJ[i])] == nullptr ||
			!chunks[_indexer(centerPosI + diffI[i], centerPosJ + diffJ[i])]->RunRaycaster(rtx))) {
			++i;
		}
		if (i < 9 && distance(m_camera.GetEye(), rtx.Curr()) <= ACTION_DISTANCE) {
			// For placing block only
			glm::vec3 prev = rtx.Prev();
			int i_right = RENDER_DISTANCE - 2;
			int j_right = RENDER_DISTANCE - 2;
			bool found = false;
			//
			switch (mouse.button) {
				case SDL_BUTTON_LEFT:
					chunks[_indexer(centerPosI + diffI[i], centerPosJ + diffJ[i])]->BreakBlock(
						rtx.Curr(),
						chunks[_indexer(centerPosI + diffI[i] - 1, centerPosJ + diffJ[i])], // north
						chunks[_indexer(centerPosI + diffI[i] + 1, centerPosJ + diffJ[i])], // south
						chunks[_indexer(centerPosI + diffI[i], centerPosJ + diffJ[i] - 1)], // west
						chunks[_indexer(centerPosI + diffI[i], centerPosJ + diffJ[i] + 1)]  // east
					);
				break;
				case SDL_BUTTON_RIGHT:
					while (i_right <= RENDER_DISTANCE && !found) {
						j_right = RENDER_DISTANCE - 2;
						while (j_right <= RENDER_DISTANCE && !found) {
							Chunk* currChunk = chunks[_indexer(i_right, j_right)];
							if (currChunk != nullptr && currChunk->isInChunk(prev)) {
								found = true;
								currChunk->PlaceBlock(
									prev,
									selectedBlock,
									chunks[_indexer(i_right - 1, j_right)], // north
									chunks[_indexer(i_right + 1, j_right)], // south
									chunks[_indexer(i_right, j_right - 1)], // west
									chunks[_indexer(i_right, j_right + 1)]  // east
								);
							}
							++j_right;
						}
						++i_right;
					}
					break;
				case SDL_BUTTON_MIDDLE:
					selectedBlock = chunks[_indexer(centerPosI + diffI[i], centerPosJ + diffJ[i])]->GetBlock(rtx.Curr());
					break;
			}
		}
		
		lockChunks.unlock();
	}
}


void MineCraft::MouseUp(SDL_MouseButtonEvent& mouse)
{
}


void MineCraft::MouseWheel(SDL_MouseWheelEvent& wheel)
{
}


void MineCraft::Resize(int _w, int _h)
{
	glViewport(0, 0, _w, _h );

	viewport_h = _h;
	viewport_w = _w;

	m_camera.Resize(_w, _h);
}


inline bool MineCraft::isInView(const glm::vec2& chunkPos)
{
	if (gm == SPECTATOR) {
		return true;
	}
	glm::vec3 pos = m_camera.GetEye();
	glm::vec2 watchTo = glm::vec2((m_camera.GetAt() - pos).x, (m_camera.GetAt() - pos).z);
	glm::vec2 toChunk = glm::vec2(chunkPos.x - pos.x, chunkPos.y - pos.z);
	float toChunkLength = length(toChunk);
	if (toChunkLength < 32.f) {
		return true;
	}
	float angle = acos(dot(toChunk, watchTo) / (toChunkLength * length(watchTo)));
	return toChunkLength < 64.f && abs(angle) < 1.5f || abs(angle) < 1.0f;
}


void MineCraft::CountFPS(float deltaTime)
{
	lastWriteTime += deltaTime;
	++ticks;
	if (lastWriteTime >= 1.0f) {
		std::cout << ticks << " FPS\n";
		lastWriteTime = 0;
		ticks = 0;
	}
}


void MineCraft::ManageGenTasks(const glm::vec3& pos, const glm::vec3& center)
{
	lockGenTasks.lock();
	// Movement to east (positive X)
	if (pos.x > center.x + 9.f) {
		for (int i = 0; i < N; ++i) {
			lockChunks.lock();
			if (chunks[_indexer(i, 0)] != nullptr) {
				delete chunks[_indexer(i, 0)];
				chunks[_indexer(i, 0)] = nullptr;
			}
			lockChunks.unlock();

			int x = 16 * (sh + RENDER_DISTANCE);
			int z = (i - RENDER_DISTANCE + 1 + sv) * 16;

			generationTasks.push({ glm::vec3(x, 0, z) });
		}
		++sh;
	}
	// Movement to west (negative X)
	else if (pos.x < center.x - 9.f) {
		for (int i = 0; i < N; ++i) {
			lockChunks.lock();
			if (chunks[_indexer(i, N - 1)] != nullptr) {
				delete chunks[_indexer(i, N - 1)];
				chunks[_indexer(i, N - 1)] = nullptr;
			}
			lockChunks.unlock();

			int x = 16 * (sh - RENDER_DISTANCE);
			int z = (i - RENDER_DISTANCE + 1 + sv) * 16;
			generationTasks.push({ glm::vec3(x, 0, z) });
		}
		--sh;
	}
	// Movement to south (positive Z)
	else if (pos.z > center.z + 9.f) {
		for (int j = 0; j < N; ++j) {
			lockChunks.lock();
			if (chunks[_indexer(0, j)] != nullptr) {
				delete chunks[_indexer(0, j)];
				chunks[_indexer(0, j)] = nullptr;
			}
			lockChunks.unlock();

			int x = (j - RENDER_DISTANCE + 1 + sh) * 16;
			int z = 16 * (sv + RENDER_DISTANCE);

			generationTasks.push({ glm::vec3(x, 0, z) });
		}
		++sv;
	}
	// Movement to north (negative Z)
	else if (pos.z < center.z - 9.f) {
		for (int j = 0; j < N; ++j) {
			lockChunks.lock();
			if (chunks[_indexer(N - 1, j)] != nullptr) {
				delete chunks[_indexer(N - 1, j)];
				chunks[_indexer(N - 1, j)] = nullptr;
			}
			lockChunks.unlock();

			int x = (j - RENDER_DISTANCE + 1 + sh) * 16;
			int z = 16 * (sv - RENDER_DISTANCE);
			generationTasks.push({ glm::vec3(x, 0, z) });
		}
		--sv;
	}
	lockGenTasks.unlock();
}


void MineCraft::Generation()	// Executed in generation thread
{
	try {
		int last_time;
		while (!isEnded) {
			if (!generationTasks.empty()) {
				last_time = SDL_GetTicks();

				lockGenTasks.lock();
				GenerationTask gt = generationTasks.front();
				generationTasks.pop();

				int i = _posToI(gt.pos), j = _posToJ(gt.pos);
				if (i >= 0 && i < N && j >= 0 && j < N && chunks[_indexer(i, j)] == nullptr) {
					Chunk* chunk = new Chunk(gt.pos);

					lockChunks.lock();
					chunks[_indexer(i, j)] = chunk;

					if (i > 0 && chunks[_indexer(i - 1, j)] != nullptr) {
						chunks[_indexer(i, j)]->GenerateConnectionWith(chunks[_indexer(i - 1, j)], Chunk::NORTH, true);
					}
					if (i < N - 1 && chunks[_indexer(i + 1, j)] != nullptr) {
						chunks[_indexer(i, j)]->GenerateConnectionWith(chunks[_indexer(i + 1, j)], Chunk::SOUTH, true);
					}
					if (j > 0 && chunks[_indexer(i, j - 1)] != nullptr) {
						chunks[_indexer(i, j)]->GenerateConnectionWith(chunks[_indexer(i, j - 1)], Chunk::WEST, true);
					}
					if (j < N - 1 && chunks[_indexer(i, j + 1)] != nullptr) {
						chunks[_indexer(i, j)]->GenerateConnectionWith(chunks[_indexer(i, j + 1)], Chunk::EAST, true);
					}
					lockChunks.unlock();

					std::cout << "Generation task done in " << SDL_GetTicks() - last_time << "ms. " << generationTasks.size() << " left.\n";
				}
				else {
					std::cout << "Generation task skipped. " << generationTasks.size() << " left\n";
				}
				lockGenTasks.unlock();
				std::this_thread::sleep_for(std::chrono::milliseconds(2)); // Give a chance to main thread to overtake
			}
		}
	}
	catch (std::exception & e) {
		std::cout << "Exception at the background thread\n" << e.what() << "\n";
	}
}