#pragma once

#include <glm/glm.hpp>
#include <cmath>

class Raycaster
{
public:

	Raycaster(glm::vec3, glm::vec3);
	~Raycaster();
	
	glm::vec3 First();
	glm::vec3 Next();
	glm::vec3 Curr();
	glm::vec3 Prev();

private:
	glm::vec3 eye, at, next, dir, curr, prev;
	float distInX = 0.f, distInY = 0.f, distInZ = 0.f;

	inline glm::vec3 GetIntersectionWithX(float);
	inline glm::vec3 GetIntersectionWithY(float);
	inline glm::vec3 GetIntersectionWithZ(float);
};

