#version 330 core

// Input from VBO
in vec3 vs_in_pos;

// Output to pipeline
out vec3 vs_out_pos;

// Uniforms
uniform mat4 MVP;

void main()
{
	gl_Position = (MVP * vec4( vs_in_pos, 1 )).xyww;

	vs_out_pos = vs_in_pos;
}