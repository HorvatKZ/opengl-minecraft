#pragma once
#include <vector>
#include <GL/glew.h>
#include <glm/gtc/noise.hpp>
#include <SDL.h>
#include <SDL_opengl.h>
#include "ChunkMesh.h";
#include "Raycaster.h"


class Chunk
{
public:
	enum Block {
		AIR, STONE, GRASS, DIRT, BEDROCK, GOLD_ORE, IRON_ORE, COAL_ORE, DIAMOND_ORE, REDSTONE_ORE, OAK_LOG, BIRCH_LOG, SPRUCE_LOG, LEAVES,
		SAND, GRAVEL, CLAY, SANDSTONE, SNOWY_GRASS, CACTUS_BLOCK, TAIGA_GRASS, SPRUCE_LEAVES
	};
	enum Decoration { NONE, RED_FLOWER, YELLOW_FLOWER, REDSHROOM, BROWNSHROOM, GRASS_DECO, SPRUCE_GRASS_DECO, BRANCH, SMALL_SPRUCE };
	enum Biome { OCEAN, BEACH, FIELD, FOREST, TAIGA, DESERT, MOUNTAIN, SNOWY_PEAK };
	enum Side { TOP, BOTTOM, LEFT, RIGHT, FRONT, BACK };
	enum Tree { OAK, BIRCH, SPRUCE, CACTUS };
	enum Dir { NORTH, SOUTH, EAST, WEST };

	Chunk(glm::vec3);
	~Chunk();

	void GenerateConnectionWith(Chunk*, Dir, bool);
	void Draw();
	void DrawTransparent();
	bool RunRaycaster(Raycaster&);

	void BreakBlock(const glm::vec3&, Chunk*, Chunk*, Chunk*, Chunk*);
	void PlaceBlock(const glm::vec3&, unsigned char, Chunk*, Chunk*, Chunk*, Chunk*);
	unsigned char GetBlock(const glm::vec3&);
	inline bool isInChunk(const glm::vec3&);

	glm::vec2 GetCenterPos();

	static unsigned char surfaces[][6];
	static unsigned char decoSurfaces[];

protected:
	glm::vec3 pos;
	char fileName[60] = "";
	unsigned char maxHeight = 0;
	unsigned char heightMap[256] = { 0 };
	unsigned char biomeMap[256] = { 0 };
	unsigned char decorations[256] = { 0 };
	unsigned char blocks[65536] = { 0 };

	inline unsigned char _indexer2(unsigned char x, unsigned char z) { return 16 * x + z; }
	inline unsigned short _indexer3(unsigned char x, unsigned char y, unsigned char z) { return 256 * y + 16 * x + z; }


	ChunkMesh mainMesh;
	ChunkMesh waterMesh;
	ChunkMesh lavaMesh;
	ChunkMesh decorationMesh;


	void Generate();
	void CreateMesh();
	void AddSurfaceTop		(unsigned char, unsigned char, unsigned char);
	void AddSurfaceBottom	(unsigned char, unsigned char, unsigned char);
	void AddSurfaceLeft		(unsigned char, unsigned char, unsigned char);
	void AddSurfaceRight	(unsigned char, unsigned char, unsigned char);
	void AddSurfaceFront	(unsigned char, unsigned char, unsigned char);
	void AddSurfaceBack		(unsigned char, unsigned char, unsigned char);


	const float WATER_HEIGHT = 61.8f;
	const unsigned char LAVA_HEIGHT = 4.8f;
	void AddWaterSurface(unsigned char, unsigned char);
	void AddLavaSurface(unsigned char, unsigned char);


	unsigned char GetHeightFor(unsigned char, unsigned char);
	bool isCave(unsigned char, unsigned char, unsigned char);
	bool isCaveEntrance(unsigned char, unsigned char, unsigned char);
	void isCaveRecursive(unsigned char, unsigned char, unsigned char);
	void isCaveEntranceRecursive(unsigned char, unsigned char, unsigned char);

	void GenerateAllOresOfType(unsigned char, unsigned char, unsigned char, unsigned char);
	void GenerateOreVein(unsigned char, unsigned char, unsigned char, unsigned char, unsigned char, unsigned char);

	char GetBiome(unsigned char, unsigned char);
	void GenerateTree(unsigned char, unsigned char, Tree);
	void GenerateLeaves(unsigned char, unsigned char, unsigned char, unsigned char, unsigned char, bool);
	void AddDecoration(unsigned char, unsigned char);

	inline char GetTexX(char, char);
	inline char GetTexY(char, char);

	inline bool isAirBlock(const glm::vec3&);
};

