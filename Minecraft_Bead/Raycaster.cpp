#include "Raycaster.h"

Raycaster::Raycaster(glm::vec3 _eye, glm::vec3 _at) : eye(_eye), at(_at)
{
	dir = glm::vec3(
		(eye.x < at.x) ? 1.f : -1.f,
		(eye.y < at.y) ? 1.f : -1.f,
		(eye.z < at.z) ? 1.f : -1.f
	);
	next = glm::vec3(
		(eye.x < at.x) ? floor(eye.x + 1.f) : floor(eye.x),
		(eye.y < at.y) ? floor(eye.y + 1.f) : floor(eye.y),
		(eye.z < at.z) ? floor(eye.z + 1.f) : floor(eye.z)
	);
	
	distInX = distance(eye, GetIntersectionWithX(next.x));
	distInY = distance(eye, GetIntersectionWithY(next.y));
	distInZ = distance(eye, GetIntersectionWithZ(next.z));

	curr = First();
	prev = curr;
}


Raycaster::~Raycaster() {}


glm::vec3 Raycaster::First()
{
	return { floor(eye.x), floor(eye.y), floor(eye.z) };
}


glm::vec3 Raycaster::Next()
{
	prev = curr;

	if (distInX < distInY) {
		if (distInX < distInZ) {
			next.x += dir.x;
			distInX = distance(eye, GetIntersectionWithX(next.x));
			curr.x += dir.x;
			return curr;
		}
		else {
			next.z += dir.z;
			distInZ = distance(eye, GetIntersectionWithZ(next.z));
			curr.z += dir.z;
			return curr;
		}
	}
	else {
		if (distInY < distInZ) {
			next.y += dir.y;
			distInY = distance(eye, GetIntersectionWithY(next.y));
			curr.y += dir.y;
			return curr;
		}
		else {
			next.z += dir.z;
			distInZ = distance(eye, GetIntersectionWithZ(next.z));
			curr.z += dir.z;
			return curr;
		}
	}
}


glm::vec3 Raycaster::Curr()
{
	return curr;
}


glm::vec3 Raycaster::Prev()
{
	return prev;
}


glm::vec3 Raycaster::GetIntersectionWithX(float x) {
	return glm::vec3(
		x,
		(x - eye.x) / (at.x - eye.x) * (at.y - eye.y) + eye.y,
		(x - eye.x) / (at.x - eye.x) * (at.z - eye.z) + eye.z
	);
}

glm::vec3 Raycaster::GetIntersectionWithY(float y) {
	return glm::vec3(
		(y - eye.y) / (at.y - eye.y) * (at.x - eye.x) + eye.x,
		y,
		(y - eye.y) / (at.y - eye.y) * (at.z - eye.z) + eye.z
	);
}

glm::vec3 Raycaster::GetIntersectionWithZ(float z) {
	return glm::vec3(
		(z - eye.z) / (at.z - eye.z) * (at.x - eye.x) + eye.x,
		(z - eye.z) / (at.z - eye.z) * (at.y - eye.y) + eye.y,
		z
	);
}