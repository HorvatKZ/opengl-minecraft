#include "Chunk.h"
#include <iostream>
#include <fstream>
#include <cstring>


Chunk::Chunk(glm::vec3 _pos) : pos(_pos)
{
	// Creating filename based on pos
	sprintf(fileName, "saves/%d_%d.bin", (int)pos.x, (int)pos.z);

	// If file exists, then read from it
	std::ifstream fs(fileName, std::ios::in | std::ios::binary);
	if (fs) {
		fs.read((char*)&maxHeight, 1);
		fs.read((char*)heightMap, 256);
		fs.read((char*)biomeMap, 256);
		fs.read((char*)decorations, 256);
		unsigned char y = 0;
		for (; y < maxHeight; ++y) {
			fs.read((char*)(blocks + 256 * y), 256);
		}
		for (; y < 255; ++y) {
			for (unsigned char x = 0; x < 16; ++x) {
				for (unsigned char z = 0; z < 16; ++z) {
					blocks[_indexer3(x, y, z)] == AIR;
				}
			}
		}
		fs.close();

		CreateMesh();
	}
	// Otherwise generate
	else {
		Generate();
	}
}


Chunk::~Chunk()
{
	// Saving chunk to file
	std::ofstream fs(fileName, std::ios::out | std::ios::binary);
	if (fs) {
		fs.write((char*)&maxHeight, 1);
		fs.write((char*)heightMap, 256);
		fs.write((char*)biomeMap, 256);
		fs.write((char*)decorations, 256);
		for (unsigned char y = 0; y < maxHeight; ++y) {
			fs.write((char*)(blocks + 256 * y), 256);
		}
		fs.close();
		printf("%s succesfully saved\n", fileName);
	}
	else {
		printf("ERROR: Failure in writing: %s\n", fileName);
	}
}


// Storing surface types for block types
unsigned char Chunk::surfaces[][6]
{
		{ 1, 1, 1, 1, 1, 1 },			// Stone
		{ 0, 2, 3, 3, 3, 3 },			// Grass
		{ 2, 2, 2, 2, 2, 2 },			// Dirt
		{ 17, 17, 17, 17, 17, 17 },		// Bedrock
		{ 32, 32, 32, 32, 32, 32 },		// Gold
		{ 33, 33, 33, 33, 33, 33 },		// Iron
		{ 34, 34, 34, 34, 34, 34 },		// Coal
		{ 50, 50, 50, 50, 50, 50 },		// Diamond
		{ 51, 51, 51, 51, 51, 51 },		// Redstone
		{ 21, 21, 20, 20, 20, 20 },		// Oak log
		{ 21, 21, 117, 117, 117, 117 },	// Birch log
		{ 21, 21, 116, 116, 116, 116 },	// Spruce log
		{ 53, 53, 53, 53, 53, 53 },		// Leaves
		{ 18, 18, 18, 18, 18, 18 },		// Sand
		{ 19, 19, 19, 19, 19, 19 },		// Gravel
		{ 72, 72, 72, 72, 72, 72 },		// Clay
		{176, 208, 192, 192, 192, 192}, // Sandstone
		{ 66, 2, 68, 68, 68, 68 },		// Snowy grass
		{ 69, 71, 70, 70, 70, 70 },		// Cactus
		{ 131, 2, 132, 132, 132, 132 },	// Taiga grass
		{ 52, 52, 52, 52, 52, 52 },		// Spruce leaves
};


unsigned char Chunk::decoSurfaces[]{ 12, 13, 28, 29, 76, 77, 78, 79 };


glm::vec2 Chunk::GetCenterPos()
{
	return glm::vec2(pos.x + 8, pos.z + 8);
}


// Generating common side with other chunk
void Chunk::GenerateConnectionWith(Chunk* other, Dir direction, bool callAgain)
{
	switch (direction) {
		case NORTH:
			for (int y = 0; y <= maxHeight; ++y) {
				for (int x = 0; x < 16; ++x) {
					if (blocks[_indexer3(x, y, 0)] != AIR && other->blocks[_indexer3(x, y, 15)] == AIR) {
						AddSurfaceBack(x, y, 0);
					}
				}
			}
			if (callAgain) {
				other->GenerateConnectionWith(this, SOUTH, false);
			}
			break;
		case SOUTH:
			for (int y = 0; y <= maxHeight; ++y) {
				for (int x = 0; x < 16; ++x) {
					if (blocks[_indexer3(x, y, 15)] != AIR && other->blocks[_indexer3(x, y, 0)] == AIR) {
						AddSurfaceFront(x, y, 15);
					}
				}
			}
			if (callAgain) {
				other->GenerateConnectionWith(this, NORTH, false);
			}
			break;
		case WEST:
			for (int y = 0; y <= maxHeight; ++y) {
				for (int z = 0; z < 16; ++z) {
					if (blocks[_indexer3(0, y, z)] != AIR && other->blocks[_indexer3(15, y, z)] == AIR) {
						AddSurfaceLeft(0, y, z);
					}
				}
			}
			if (callAgain) {
				other->GenerateConnectionWith(this, EAST, false);
			}
			break;
		case EAST:
			for (int y = 0; y <= maxHeight; ++y) {
				for (int z = 0; z < 16; ++z) {
					if (blocks[_indexer3(15, y, z)] != AIR && other->blocks[_indexer3(0, y, z)] == AIR) {
						AddSurfaceRight(15, y, z);
					}
				}
			}
			if (callAgain) {
				other->GenerateConnectionWith(this, WEST, false);
			}
			break;
	}
}


// Generate chunk from scratch
void Chunk::Generate()
{
	// Generating terrain
	for (unsigned char x = 0; x < 16; ++x) {
		for (unsigned char z = 0; z < 16; ++z) {
			// Generating heightMap and biomeMap
			heightMap[_indexer2(x, z)] = GetHeightFor(x, z);
			if (heightMap[_indexer2(x, z)] > maxHeight) {
				maxHeight = heightMap[_indexer2(x, z)];
			}
			biomeMap[_indexer2(x, z)] = GetBiome(x, z);

			// Generating bedrocks and stones
			blocks[_indexer3(x, 0, z)] = BEDROCK;
			blocks[_indexer3(x, 1, z)] = (rand() % 2 > 0) ? STONE : BEDROCK;
			blocks[_indexer3(x, 2, z)] = (rand() % 5 > 0) ? STONE : BEDROCK;
			unsigned char y = 3;
			for (; y < heightMap[_indexer2(x, z)] - 4; ++y) {
				blocks[_indexer3(x, y, z)] = STONE;
			}

			// Generating surface according to biomeMap
			switch (biomeMap[_indexer2(x, z)]) {
				case OCEAN:
					blocks[_indexer3(x, y++, z)] = SANDSTONE;
					blocks[_indexer3(x, y++, z)] = SAND;
					blocks[_indexer3(x, y++, z)] = SAND;
					switch (rand() % 3) {
						case 0:
							blocks[_indexer3(x, y++, z)] = SAND;
							break;
						case 1:
							blocks[_indexer3(x, y++, z)] = GRAVEL;
							break;
						case 2:
							blocks[_indexer3(x, y++, z)] = CLAY;
							break;
					}
					break;
				case BEACH: case DESERT:
					if (!isCaveEntrance(x, y, z)) {
						blocks[_indexer3(x, y++, z)] = SANDSTONE;
					}
					if (!isCaveEntrance(x, y, z)) {
						blocks[_indexer3(x, y++, z)] = SAND;
					}
					if (!isCaveEntrance(x, y, z)) {
						blocks[_indexer3(x, y++, z)] = SAND;
					}
					if (!isCaveEntrance(x, y, z)) {
						blocks[_indexer3(x, y++, z)] = SAND;
					}
					break;
				case FIELD: case FOREST: case TAIGA: case SNOWY_PEAK:
					if (!isCaveEntrance(x, y, z)) {
						blocks[_indexer3(x, y++, z)] = DIRT;
					}
					if (!isCaveEntrance(x, y, z)) {
						blocks[_indexer3(x, y++, z)] = DIRT;
					}
					if (!isCaveEntrance(x, y, z)) {
						blocks[_indexer3(x, y++, z)] = DIRT;
					}
					switch (biomeMap[_indexer2(x, z)]) {
						case FIELD: case FOREST:
							if (!isCaveEntrance(x, y, z)) {
								blocks[_indexer3(x, y++, z)] = GRASS;
							}
							break;
						case TAIGA:
							if (!isCaveEntrance(x, y, z)) {
								blocks[_indexer3(x, y++, z)] = TAIGA_GRASS;
							}
							break;
						case SNOWY_PEAK:
							if (!isCaveEntrance(x, y, z)) {
								blocks[_indexer3(x, y++, z)] = SNOWY_GRASS;
							}
							break;
					}
					break;
				case MOUNTAIN:
					blocks[_indexer3(x, y++, z)] = STONE;
					blocks[_indexer3(x, y++, z)] = STONE;
					blocks[_indexer3(x, y++, z)] = STONE;
					blocks[_indexer3(x, y++, z)] = STONE;
					break;
			}

			// Filling up the rest with air
			for (; y < 255; ++y) {
				blocks[_indexer3(x, y, z)] = AIR;
			}
		}
	}

	// Cave generation
	for (unsigned char x = 4; x < 16; x += 8) {
		for (unsigned char z = 4; z < 16; z += 8) {
			unsigned char y = 1;
			while (y < heightMap[_indexer2(x, z)] - 4) {
				if (blocks[_indexer3(x, y, z)] != BEDROCK) {
					isCaveRecursive(x, y, z);
				}
				y += 8;
			}
			y = heightMap[_indexer2(x, z)] - 2;
			isCaveEntranceRecursive(x, y, z);
		}
	}

	// Generation ore veins
	GenerateAllOresOfType(GOLD_ORE, 4, 8, 32);
	GenerateAllOresOfType(IRON_ORE, 16, 16, 64);
	GenerateAllOresOfType(COAL_ORE, 60, 32, 128);
	GenerateAllOresOfType(DIAMOND_ORE, 1, 4, 16);
	GenerateAllOresOfType(REDSTONE_ORE, 4, 4, 16);

	// Generating trees
	for (unsigned char x = 1; x < 15; ++x) {
		for (unsigned char z = 1; z < 15; ++z) {
			switch (biomeMap[_indexer2(x, z)]) {
				case FOREST:
					if (blocks[_indexer3(x,heightMap[_indexer2(x, z)] - 1, z)] == GRASS && rand() % 16 == 0) {
						GenerateTree(x, z, (Tree)(rand() % 2)); // Oak or birch
					}
					break;
				case TAIGA:
					if (blocks[_indexer3(x, heightMap[_indexer2(x, z)] - 1, z)] == TAIGA_GRASS && rand() % 20 == 0) {
						GenerateTree(x, z, SPRUCE);
					}
					break;
				case DESERT:
					if (blocks[_indexer3(x, heightMap[_indexer2(x, z)] - 1, z)] == SAND && rand() % 256 == 0) {
						GenerateTree(x, z, CACTUS);
					}
					break;
				case SNOWY_PEAK:
					if (blocks[_indexer3(x, heightMap[_indexer2(x, z)] - 1, z)] == SNOWY_GRASS && rand() % 500 == 0) {
						GenerateTree(x, z, SPRUCE);
					}
					break;
			}
		}
	}

	// Generating decoration
	for (unsigned char x = 0; x < 16; ++x) {
		for (unsigned char z = 0; z < 16; ++z) {
			char upperBlock = blocks[_indexer3(x, heightMap[_indexer2(x, z)] - 1, z)];
			if ((upperBlock == GRASS || upperBlock == TAIGA_GRASS || upperBlock == SAND) && blocks[_indexer3(x, heightMap[_indexer2(x, z)], z)] == AIR) {
				int a;
				switch (biomeMap[_indexer2(x, z)]) {
				case DESERT:
					decorations[_indexer2(x, z)] = (rand() % 256 == 0) ? BRANCH : NONE;
					break;
				case FIELD:
					a = rand() % 256;
					if (a < 2) {
						decorations[_indexer2(x, z)] = YELLOW_FLOWER;
					}
					else if (a < 5) {
						decorations[_indexer2(x, z)] = RED_FLOWER;
					}
					else if (a < 50) {
						decorations[_indexer2(x, z)] = GRASS_DECO;
					}
					else {
						decorations[_indexer2(x, z)] = NONE;
					}
					break;
				case FOREST:
					a = rand() % 256;
					if (a < 2) {
						decorations[_indexer2(x, z)] = BROWNSHROOM;
					}
					else if (a < 5) {
						decorations[_indexer2(x, z)] = REDSHROOM;
					}
					else if (a < 20) {
						decorations[_indexer2(x, z)] = GRASS_DECO;
					}
					else {
						decorations[_indexer2(x, z)] = NONE;
					}
					break;
				case TAIGA:
					a = rand() % 256;
					if (a < 3) {
						decorations[_indexer2(x, z)] = REDSHROOM;
					}
					else if (a < 7) {
						decorations[_indexer2(x, z)] = BROWNSHROOM;
					}
					else if (a < 20) {
						decorations[_indexer2(x, z)] = SMALL_SPRUCE;
					}
					else if (a < 50) {
						decorations[_indexer2(x, z)] = SPRUCE_GRASS_DECO;
					}
					else {
						decorations[_indexer2(x, z)] = NONE;
					}
					break;
				}
			}
		}
	}

	CreateMesh();
}


void Chunk::CreateMesh()
{
	// Add block surfaces
	for (unsigned char y = 0; y <= maxHeight; ++y) {
		for (unsigned char x = 0; x < 16; ++x) {
			for (unsigned char z = 0; z < 16; ++z) {
				if (blocks[_indexer3(x, y, z)] != AIR) {
					if (x < 15 && blocks[_indexer3(x + 1, y, z)] == AIR) {
						AddSurfaceRight(x, y, z);
					}
					if (x > 0 && blocks[_indexer3(x - 1, y, z)] == AIR) {
						AddSurfaceLeft(x, y, z);
					}
					if (z < 15 && blocks[_indexer3(x, y, z + 1)] == AIR) {
						AddSurfaceFront(x, y, z);
					}
					if (z > 0 && blocks[_indexer3(x, y, z - 1)] == AIR) {
						AddSurfaceBack(x, y, z);
					}
					if (y < 255 && blocks[_indexer3(x, y + 1, z)] == AIR || y == 255) {
						AddSurfaceTop(x, y, z);
					}
					if (y > 0 && blocks[_indexer3(x, y - 1, z)] == AIR || y == 0) {
						AddSurfaceBottom(x, y, z);
					}
				}
			}
		}
	}

	// Add decoration, water and lava surfaces
	for (unsigned char x = 0; x < 16; ++x) {
		for (unsigned char z = 0; z < 16; ++z) {
			if (heightMap[_indexer2(x, z)] < WATER_HEIGHT) {
				AddWaterSurface(x, z);
			}
			if (blocks[_indexer3(x, 3, z)] == AIR) {
				AddLavaSurface(x, z);
			}
			if (decorations[_indexer2(x, z)] != NONE) {
				AddDecoration(x, z);
			}
		}
	}
}


void Chunk::Draw()
{
	mainMesh.Draw();
	lavaMesh.Draw();
}


void Chunk::DrawTransparent()
{
	waterMesh.Draw();
	decorationMesh.Draw();
}


void Chunk::AddSurfaceTop(unsigned char x, unsigned char y, unsigned char z)
{
	char textureIndex = surfaces[blocks[_indexer3(x, y, z)] - 1][TOP];
	mainMesh.AddSurface({
		{
			{ pos + glm::vec3(x,     y + 1, z),     GetTexX(textureIndex, 0), GetTexY(textureIndex, 0), TOP },
			{ pos + glm::vec3(x,     y + 1, z + 1), GetTexX(textureIndex, 1), GetTexY(textureIndex, 1), TOP },
			{ pos + glm::vec3(x + 1, y + 1, z + 1), GetTexX(textureIndex, 2), GetTexY(textureIndex, 2), TOP },
			{ pos + glm::vec3(x + 1, y + 1, z),     GetTexX(textureIndex, 3), GetTexY(textureIndex, 3), TOP }
		},
		{ 0, 1, 2, 2, 3, 0 },
		{ _indexer3(x, y, z), TOP }
	});
}


void Chunk::AddSurfaceBottom(unsigned char x, unsigned char y, unsigned char z)
{
	char textureIndex = surfaces[blocks[_indexer3(x, y, z)] - 1][BOTTOM];
	mainMesh.AddSurface({
		{
			{ pos + glm::vec3(x,     y, z),     GetTexX(textureIndex, 0), GetTexY(textureIndex, 0), BOTTOM },
			{ pos + glm::vec3(x + 1, y, z),     GetTexX(textureIndex, 1), GetTexY(textureIndex, 1), BOTTOM },
			{ pos + glm::vec3(x + 1, y, z + 1), GetTexX(textureIndex, 2), GetTexY(textureIndex, 2), BOTTOM },
			{ pos + glm::vec3(x,     y, z + 1), GetTexX(textureIndex, 3), GetTexY(textureIndex, 3), BOTTOM }
		},
		{ 0, 1, 2, 2, 3, 0 },
		{ _indexer3(x, y, z), BOTTOM }
	});
}


void Chunk::AddSurfaceLeft(unsigned char x, unsigned char y, unsigned char z)
{
	char textureIndex = surfaces[blocks[_indexer3(x, y, z)] - 1][LEFT];
	mainMesh.AddSurface({
		{
			{ pos + glm::vec3(x, y,     z),     GetTexX(textureIndex, 1), GetTexY(textureIndex, 1), LEFT },
			{ pos + glm::vec3(x, y,     z + 1), GetTexX(textureIndex, 2), GetTexY(textureIndex, 2), LEFT },
			{ pos + glm::vec3(x, y + 1, z + 1), GetTexX(textureIndex, 3), GetTexY(textureIndex, 3), LEFT },
			{ pos + glm::vec3(x, y + 1, z),     GetTexX(textureIndex, 0), GetTexY(textureIndex, 0), LEFT }
		},
		{ 0, 1, 2, 2, 3, 0 },
		{ _indexer3(x, y, z), LEFT }
	});
}


void Chunk::AddSurfaceRight(unsigned char x, unsigned char y, unsigned char z)
{
	char textureIndex = surfaces[blocks[_indexer3(x, y, z)] - 1][RIGHT];
	mainMesh.AddSurface({
		{
			{ pos + glm::vec3(x + 1, y,     z),     GetTexX(textureIndex, 2), GetTexY(textureIndex, 2), RIGHT },
			{ pos + glm::vec3(x + 1, y + 1, z),     GetTexX(textureIndex, 3), GetTexY(textureIndex, 3), RIGHT },
			{ pos + glm::vec3(x + 1, y + 1, z + 1), GetTexX(textureIndex, 0), GetTexY(textureIndex, 0), RIGHT },
			{ pos + glm::vec3(x + 1, y,     z + 1), GetTexX(textureIndex, 1), GetTexY(textureIndex, 1), RIGHT }
		},
		{ 0, 1, 2, 2, 3, 0 },
		{ _indexer3(x, y, z), RIGHT }
	});
}


void Chunk::AddSurfaceFront(unsigned char x, unsigned char y, unsigned char z)
{
	char textureIndex = surfaces[blocks[_indexer3(x, y, z)] - 1][FRONT];
	mainMesh.AddSurface({
		{
			{ pos + glm::vec3(x,     y,     z + 1), GetTexX(textureIndex, 1), GetTexY(textureIndex, 1), FRONT },
			{ pos + glm::vec3(x + 1, y,     z + 1), GetTexX(textureIndex, 2), GetTexY(textureIndex, 2), FRONT },
			{ pos + glm::vec3(x + 1, y + 1, z + 1), GetTexX(textureIndex, 3), GetTexY(textureIndex, 3), FRONT },
			{ pos + glm::vec3(x,     y + 1, z + 1), GetTexX(textureIndex, 0), GetTexY(textureIndex, 0), FRONT }
		},
		{ 0, 1, 2, 2, 3, 0 },
		{ _indexer3(x, y, z), FRONT }
	});
}


void Chunk::AddSurfaceBack(unsigned char x, unsigned char y, unsigned char z)
{
	char textureIndex = surfaces[blocks[_indexer3(x, y, z)] - 1][BACK];
	mainMesh.AddSurface({
		{
			{ pos + glm::vec3(x,     y,     z), GetTexX(textureIndex, 2), GetTexY(textureIndex, 2), BACK },
			{ pos + glm::vec3(x,     y + 1, z), GetTexX(textureIndex, 3), GetTexY(textureIndex, 3), BACK },
			{ pos + glm::vec3(x + 1, y + 1, z), GetTexX(textureIndex, 0), GetTexY(textureIndex, 0), BACK },
			{ pos + glm::vec3(x + 1, y,     z), GetTexX(textureIndex, 1), GetTexY(textureIndex, 1), BACK }
		},
		{ 0, 1, 2, 2, 3, 0 },
		{ _indexer3(x, y, z), BACK }
	});
}


void Chunk::AddWaterSurface(unsigned char x, unsigned char z)
{
	waterMesh.AddSurface({
		{
			{ pos + glm::vec3(x,     WATER_HEIGHT, z),     15, 4, 6 },
			{ pos + glm::vec3(x,     WATER_HEIGHT, z + 1), 15, 3, 6 },
			{ pos + glm::vec3(x + 1, WATER_HEIGHT, z + 1), 16, 3, 6 },
			{ pos + glm::vec3(x + 1, WATER_HEIGHT, z),     16, 4, 6 },
		},
		{ 0, 1, 2, 2, 3, 0 },
		{ _indexer2(x, z), TOP }
	});
}


void Chunk::AddLavaSurface(unsigned char x, unsigned char z)
{
	lavaMesh.AddSurface({
		{
			{ pos + glm::vec3(x,     LAVA_HEIGHT, z),     15, 2, 0 },
			{ pos + glm::vec3(x,     LAVA_HEIGHT, z + 1), 15, 1, 0 },
			{ pos + glm::vec3(x + 1, LAVA_HEIGHT, z + 1), 16, 1, 0 },
			{ pos + glm::vec3(x + 1, LAVA_HEIGHT, z),     16, 2, 0 }
		},
		{ 0, 1, 2, 2, 3, 0 },
		{ _indexer2(x, z), TOP }
	});
}


void Chunk::AddDecoration(unsigned char x, unsigned char z)
{
	char textureIndex = decoSurfaces[decorations[_indexer2(x, z)] - 1];
	unsigned char y = heightMap[_indexer2(x, z)];
	decorationMesh.AddSurface({
		{
			{ pos + glm::vec3(x,     y,     z),     GetTexX(textureIndex, 1), GetTexY(textureIndex, 1), 4 },
			{ pos + glm::vec3(x + 1, y,     z + 1), GetTexX(textureIndex, 2), GetTexY(textureIndex, 2), 4 },
			{ pos + glm::vec3(x + 1, y + 1, z + 1), GetTexX(textureIndex, 3), GetTexY(textureIndex, 3), 4 },
			{ pos + glm::vec3(x,     y + 1, z),     GetTexX(textureIndex, 0), GetTexY(textureIndex, 0), 4 }
		},
		{ 0, 1, 2, 2, 3, 0 },
		{ _indexer2(x, z), 0 }
	});

	decorationMesh.AddSurface({
		{
			{ pos + glm::vec3(x,     y,     z),     GetTexX(textureIndex, 1), GetTexY(textureIndex, 1), 4 },
			{ pos + glm::vec3(x,     y + 1, z),     GetTexX(textureIndex, 0), GetTexY(textureIndex, 0), 4 },
			{ pos + glm::vec3(x + 1, y + 1, z + 1), GetTexX(textureIndex, 3), GetTexY(textureIndex, 3), 4 },
			{ pos + glm::vec3(x + 1, y,     z + 1), GetTexX(textureIndex, 2), GetTexY(textureIndex, 2), 4 }
		},
		{ 0, 1, 2, 2, 3, 0 },
		{ _indexer2(x, z), 1 }
		});

	decorationMesh.AddSurface({
		{
			{ pos + glm::vec3(x,     y,     z + 1), GetTexX(textureIndex, 1), GetTexY(textureIndex, 1), 4 },
			{ pos + glm::vec3(x + 1, y,     z),     GetTexX(textureIndex, 2), GetTexY(textureIndex, 2), 4 },
			{ pos + glm::vec3(x + 1, y + 1, z),     GetTexX(textureIndex, 3), GetTexY(textureIndex, 3), 4 },
			{ pos + glm::vec3(x,     y + 1, z + 1), GetTexX(textureIndex, 0), GetTexY(textureIndex, 0), 4 }
		},
		{ 0, 1, 2, 2, 3, 0 },
		{ _indexer2(x, z), 2 }
		});

	decorationMesh.AddSurface({
		{
			{ pos + glm::vec3(x,     y,     z + 1), GetTexX(textureIndex, 1), GetTexY(textureIndex, 1), 4 },
			{ pos + glm::vec3(x,     y + 1, z + 1), GetTexX(textureIndex, 0), GetTexY(textureIndex, 0), 4 },
			{ pos + glm::vec3(x + 1, y + 1, z),     GetTexX(textureIndex, 3), GetTexY(textureIndex, 3), 4 },
			{ pos + glm::vec3(x + 1, y,     z),     GetTexX(textureIndex, 2), GetTexY(textureIndex, 2), 4 }
		},
		{ 0, 1, 2, 2, 3, 0 },
		{ _indexer2(x, z), 3 }
		});
}


char Chunk::GetTexX(char ind, char s)
{
	if (s < 2) {
		return ind % 16;
	}
	return ind % 16 + 1;
}


char Chunk::GetTexY(char ind, char s)
{
	if (s == 1 || s == 2) {
		return 15 - (ind / 16);
	}
	return 16 - (ind / 16);
}


unsigned char Chunk::GetHeightFor(unsigned char x, unsigned char z)
{
	return glm::simplex(glm::vec2((pos.x + x) / 512.f, (pos.z + z) / 512.f)) * 50 +
		glm::simplex(glm::vec2((pos.x + x) / 128.f, (pos.z + z) / 128.f)) * 10 + 
		glm::simplex(glm::vec2((pos.x + x) / 32.f, (pos.z + z) / 32.f)) * 2 + 80;
}


bool Chunk::isCave(unsigned char x, unsigned char y, unsigned char z)
{
	float a = glm::simplex(glm::vec2((pos.x + x) / 16.f, (pos.y + y) / 16.f));
	float b = glm::simplex(glm::vec2((pos.x + x) / 16.f, (pos.z + z) / 16.f));
	float c = glm::simplex(glm::vec2((pos.x + x) / -16.f, (pos.y + y) / -16.f));
	float d = glm::simplex(glm::vec2((pos.x + x) / -16.f, (pos.z + z) / -16.f));
	return (a + b) > 1.0f || (c + d) > 1.0f;
}


bool Chunk::isCaveEntrance(unsigned char x, unsigned char y, unsigned char z)
{
	float a = glm::simplex(glm::vec2((pos.x + x) / 16.f, (pos.y + y) / 16.f));
	float b = glm::simplex(glm::vec2((pos.x + x) / 16.f, (pos.z + z) / 16.f));
	return (a + b) > 1.4f;
}


void Chunk::isCaveRecursive(unsigned char x, unsigned char y, unsigned char z)
{
	if (blocks[_indexer3(x, y, z)] == STONE && isCave(x, y, z)) {
		blocks[_indexer3(x, y, z)] = AIR;
		if (y > 0 && blocks[_indexer3(x, y - 1, z)] == STONE) {
			isCaveRecursive(x, y - 1, z);
		}
		if (y < 255 && blocks[_indexer3(x, y + 1, z)] == STONE) {
			isCaveRecursive(x, y + 1, z);
		}
		if (x > 0 && blocks[_indexer3(x - 1, y, z)] == STONE) {
			isCaveRecursive(x - 1, y, z);
		}
		if (x < 15 && blocks[_indexer3(x + 1, y, z)] == STONE) {
			isCaveRecursive(x + 1, y, z);
		}
		if (z > 0 && blocks[_indexer3(x, y, z - 1)] == STONE) {
			isCaveRecursive(x, y, z - 1);
		}
		if (z < 15 && blocks[_indexer3(x, y, z + 1)] == STONE) {
			isCaveRecursive(x, y, z + 1);
		}
	}
}


void Chunk::isCaveEntranceRecursive(unsigned char x, unsigned char y, unsigned char z)
{
	if (blocks[_indexer3(x, y, z)] != AIR && isCaveEntrance(x, y, z)) {
		blocks[_indexer3(x, y, z)] = AIR;
		if (y > 0) {
			isCaveEntranceRecursive(x, y - 1, z);
		}
		if (y < 255) {
			isCaveEntranceRecursive(x, y + 1, z);
		}
		if (x > 0) {
			isCaveEntranceRecursive(x - 1, y, z);
		}
		if (x < 15) {
			isCaveEntranceRecursive(x + 1, y, z);
		}
		if (z > 0) {
			isCaveEntranceRecursive(x, y, z - 1);
		}
		if (z < 15) {
			isCaveEntranceRecursive(x, y, z + 1);
		}
	}
}


void Chunk::GenerateAllOresOfType(unsigned char type, unsigned char veinQuantity, unsigned char inOneVein, unsigned char maxHeight)
{
	for (int i = 0; i < veinQuantity; ++i) {
		GenerateOreVein(rand() % 16, rand() % maxHeight, rand() % 16, type, inOneVein, maxHeight);
	}
}


void Chunk::GenerateOreVein(unsigned char x, unsigned char y, unsigned char z, unsigned char type, unsigned char inOneVein, unsigned char maxHeight)
{
	if (blocks[_indexer3(x, y, z)] == STONE && inOneVein > 0) {
		blocks[_indexer3(x, y, z)] = type;
		if (x > 0 && rand() % 2 == 0) {
			GenerateOreVein(x - 1, y, z, type, inOneVein / 3, maxHeight);
		}
		if (x < 15 && rand() % 2 == 0) {
			GenerateOreVein(x + 1, y, z, type, inOneVein / 3, maxHeight);
		}
		if (z > 0 && rand() % 2 == 0) {
			GenerateOreVein(x, y, z - 1, type, inOneVein / 3, maxHeight);
		}
		if (z < 15 && rand() % 2 == 0) {
			GenerateOreVein(x, y, z + 1, type, inOneVein / 3, maxHeight);
		}
		if (y > 1 && rand() % 2 == 0) {
			GenerateOreVein(x, y - 1, z, type, inOneVein / 3, maxHeight);
		}
		if (y < maxHeight - 1 && rand() % 2 == 0) {
			GenerateOreVein(x, y + 1, z, type, inOneVein / 3, maxHeight);
		}
	}
}


void Chunk::GenerateTree(unsigned char x, unsigned char z, Tree tree)
{
	int height, heightRand, block;
	switch (tree) {
		case OAK:
			height = 5;
			heightRand = 2;
			block = OAK_LOG;
			break;
		case BIRCH:
			height = 6;
			heightRand = 3;
			block = BIRCH_LOG;
			break;
		case SPRUCE:
			height = 6;
			heightRand = 4;
			block = SPRUCE_LOG;
			break;
		case CACTUS:
			height = 3;
			heightRand = 2;
			block = CACTUS_BLOCK;
			break;
	}

	unsigned char max = heightMap[_indexer2(x, z)] + rand() % heightRand + height;
	if (max > maxHeight) {
		maxHeight = max;
	}
	unsigned char y = heightMap[_indexer2(x, z)];
	for (; y < max; ++y) {
		blocks[_indexer3(x, y, z)] = block;
	}
	
	if (tree == OAK || tree == BIRCH) {
		GenerateLeaves(x, y - 1, z, 5, 3, false);
	}
	else if (tree == SPRUCE) {
		GenerateLeaves(x, y - 1, z, 5, 1, true);
		GenerateLeaves(x, y - 2, z, 5, 2, true);
		GenerateLeaves(x, y - 4, z, 5, 3, true);
	}
}


void Chunk::GenerateLeaves(unsigned char x, unsigned char y, unsigned char z, unsigned char chance, unsigned char dist, bool isSpruce)
{
	if (blocks[_indexer3(x, y, z)] == AIR) {
		if (isSpruce) {
			blocks[_indexer3(x, y, z)] = SPRUCE_LEAVES;
		}
		else {
			blocks[_indexer3(x, y, z)] = LEAVES;
		}
		
		if (y > maxHeight - 1) {
			maxHeight = y + 1;
		}
	}
	if (dist > 0) {
		if (x > 0 && rand() % 6 < chance) {
			GenerateLeaves(x - 1, y, z, chance, dist - 1, isSpruce);
		}
		if (x < 15 && rand() % 6 < chance) {
			GenerateLeaves(x + 1, y, z, chance, dist - 1, isSpruce);
		}
		if (z > 0 && rand() % 6 < chance) {
			GenerateLeaves(x, y, z - 1, chance, dist - 1, isSpruce);
		}
		if (z < 15 && rand() % 6 < chance) {
			GenerateLeaves(x, y, z + 1, chance, dist - 1, isSpruce);
		}
		if (rand() % 6 < chance) {
			GenerateLeaves(x, y - 1, z, chance, dist - 1, isSpruce);
		}
		if (rand() % 6 < chance) {
			GenerateLeaves(x, y + 1, z, chance, dist - 1, isSpruce);
		}
	}
}


char Chunk::GetBiome(unsigned char x, unsigned char z)
{
	if (heightMap[_indexer2(x, z)] < 60) {
		return OCEAN;
	}
	else if (heightMap[_indexer2(x, z)] < 65) {
		return BEACH;
	}
	else if (heightMap[_indexer2(x, z)] < 110) {
		float val = glm::simplex(glm::vec2((pos.x + x) / 1024.f, (pos.z + z) / 1024.f));
		if (val < 0.f) {
			if (val < -0.4f) {
				return DESERT;
			}
			else {
				return FIELD;
			}
		}
		else {
			if (val < 0.4f) {
				return FOREST;
			}
			else {
				return TAIGA;
			}
		}
	}
	else {
		float val = glm::simplex(glm::vec2((pos.x + x) / 256.f, (pos.z + z) / 256.f));
		if (val < 0.3f) {
			return SNOWY_PEAK;
		}
		else {
			return MOUNTAIN;
		}
	}
}


bool Chunk::RunRaycaster(Raycaster& rtx)
{
	glm::vec3 curr = rtx.Curr();
	while (isInChunk(curr) && isAirBlock(curr)) {
		curr = rtx.Next();
	}
	return isInChunk(curr);
}


void Chunk::BreakBlock(const glm::vec3& blockPos, Chunk* north, Chunk* south, Chunk* west, Chunk* east)
{
	unsigned char x = (char)floor(blockPos.x - pos.x);
	unsigned char y = (char)floor(blockPos.y);
	unsigned char z = (char)floor(blockPos.z - pos.z);
	unsigned short ind = _indexer3(x, y, z);
	blocks[ind] = AIR;

	// Break decoration
	unsigned char indDec = _indexer2(x, z);
	if (y == heightMap[_indexer2(x, z)] - 1 && decorations[indDec] != NONE) {
		decorations[indDec] = NONE;
		
		for (unsigned char i = 0; i < 4; ++i) {
			int result = decorationMesh.FindSurface({ indDec, i });
			if (result != -1) {
				decorationMesh.DeleteSurface(result);
			}
		}
	}

	// Lava flow
	if (y == 3) {
		AddLavaSurface(x, z);
	}
	
	// Break block
	for (unsigned char side = TOP; side <= BACK; ++side) {
		int result = mainMesh.FindSurface({ ind, side });
		if (result != -1) {	// Delete surface if found
			mainMesh.DeleteSurface(result);
		}
		else { // Create surface for other block otherwise
			switch (side) {
				case TOP:
					if (y < 255) {
						AddSurfaceBottom(x, y + 1, z);
					}
					break;
				case BOTTOM:
					if (y > 0) {
						AddSurfaceTop(x, y - 1, z);
					}
					break;
				case LEFT:
					if (x > 0) {
						AddSurfaceRight(x - 1, y, z);
					}
					else if (west != nullptr) {
						west->AddSurfaceRight(15, y, z);
					}
					break;
				case RIGHT:
					if (x < 15) {
						AddSurfaceLeft(x + 1, y, z);
					}
					else if (east != nullptr) {
						east->AddSurfaceLeft(0, y, z);
					}
					break;
				case FRONT:
					if (z < 15) {
						AddSurfaceBack(x, y, z + 1);
					}
					else if (south != nullptr) {
						south->AddSurfaceBack(x, y, 0);
					}
					break;
				case BACK:
					if (z > 0) {
						AddSurfaceFront(x, y, z - 1);
					}
					else if (north != nullptr) {
						north->AddSurfaceFront(x, y, 15);
					}
					break;
			}
		}
	}
}


void Chunk::PlaceBlock(const glm::vec3& blockPos, unsigned char block, Chunk* north, Chunk* south, Chunk* west, Chunk* east)
{
	unsigned char x = (char)floor(blockPos.x - pos.x);
	unsigned char y = (char)floor(blockPos.y);
	unsigned char z = (char)floor(blockPos.z - pos.z);
	blocks[_indexer3(x, y, z)] = block;

	if (y > maxHeight + 1) {
		maxHeight = y + 1;
	}

	// Break decoration
	unsigned char indDec = _indexer2(x, z);
	if (y == heightMap[_indexer2(x, z)] && decorations[indDec] != NONE) {
		decorations[indDec] = NONE;

		for (unsigned char i = 0; i < 4; ++i) {
			int result = decorationMesh.FindSurface({ indDec, i });
			if (result != -1) {
				decorationMesh.DeleteSurface(result);
			}
		}
	}

	// Lava unflow
	if (y == 3) {
		lavaMesh.DeleteSurface(lavaMesh.FindSurface({ _indexer2(x, z), TOP }));
	}

	// Top surface
	if (y < 255) {
		int result = mainMesh.FindSurface({ _indexer3(x, y + 1, z), BOTTOM });
		if (result != -1) { // Delete surface if found
			mainMesh.DeleteSurface(result);
		}
		else {
			AddSurfaceTop(x, y, z);
		}
	}
	else {
		AddSurfaceTop(x, y, z);
	}

	// Bottom surface
	if (y > 0) {
		int result = mainMesh.FindSurface({ _indexer3(x, y - 1, z), TOP });
		if (result != -1) { // Delete surface if found
			mainMesh.DeleteSurface(result);
		}
		else {
			AddSurfaceBottom(x, y, z);
		}
	}
	else {
		AddSurfaceBottom(x, y, z);
	}

	// Left surface
	if (x > 0) {
		int result = mainMesh.FindSurface({ _indexer3(x - 1, y, z), RIGHT });
		if (result != -1) { // Delete surface if found
			mainMesh.DeleteSurface(result);
		}
		else {
			AddSurfaceLeft(x, y, z);
		}
	}
	else if (west != nullptr) {
		int result = west->mainMesh.FindSurface({ _indexer3(15, y, z), RIGHT });
		if (result != -1) { // Delete surface if found
			west->mainMesh.DeleteSurface(result);
		}
		else {
			AddSurfaceLeft(x, y, z);
		}
	}

	// Right surface
	if (x < 15) {
		int result = mainMesh.FindSurface({ _indexer3(x + 1, y, z), LEFT });
		if (result != -1) { // Delete surface if found
			mainMesh.DeleteSurface(result);
		}
		else {
			AddSurfaceRight(x, y, z);
		}
	}
	else if (east != nullptr) {
		int result = east->mainMesh.FindSurface({ _indexer3(0, y, z), LEFT });
		if (result != -1) { // Delete surface if found
			east->mainMesh.DeleteSurface(result);
		}
		else {
			AddSurfaceRight(x, y, z);
		}
	}
	
	// Front surface
	if (z < 15) {
		int result = mainMesh.FindSurface({ _indexer3(x, y, z + 1), BACK });
		if (result != -1) { // Delete surface if found
			mainMesh.DeleteSurface(result);
		}
		else {
			AddSurfaceFront(x, y, z);
		}
	}
	else if (south != nullptr) {
		int result = south->mainMesh.FindSurface({ _indexer3(x, y, 0), BACK });
		if (result != -1) { // Delete surface if found
			south->mainMesh.DeleteSurface(result);
		}
		else {
			AddSurfaceFront(x, y, z);
		}
	}

	// Back surface
	if (z > 0) {
		int result = mainMesh.FindSurface({ _indexer3(x, y, z - 1), FRONT });
		if (result != -1) { // Delete surface if found
			mainMesh.DeleteSurface(result);
		}
		else {
			AddSurfaceBack(x, y, z);
		}
	}
	else if (north != nullptr) {
		int result = north->mainMesh.FindSurface({ _indexer3(x, y, 15), FRONT });
		if (result != -1) { // Delete surface if found
			north->mainMesh.DeleteSurface(result);
		}
		else {
			AddSurfaceBack(x, y, z);
		}
	}
}


unsigned char Chunk::GetBlock(const glm::vec3& blockPos)
{
	unsigned char x = (char)floor(blockPos.x - pos.x);
	unsigned char y = (char)floor(blockPos.y);
	unsigned char z = (char)floor(blockPos.z - pos.z);

	return blocks[_indexer3(x, y, z)];
}


bool Chunk::isInChunk(const glm::vec3& vec)
{
	return 
		vec.x >= pos.x && vec.x < pos.x + 16.f &&
		vec.y >= 0.f && vec.y < 255.f &&
		vec.z >= pos.z && vec.z < pos.z + 16.f;
}


bool Chunk::isAirBlock(const glm::vec3& vec)
{
	unsigned char x = (char)floor(vec.x - pos.x);
	unsigned char y = (char)floor(vec.y);
	unsigned char z = (char)floor(vec.z - pos.z);

	return blocks[_indexer3(x, y, z)] == AIR;
}