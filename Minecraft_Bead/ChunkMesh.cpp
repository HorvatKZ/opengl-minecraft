#include "ChunkMesh.h"


ChunkMesh::ChunkMesh() {}


ChunkMesh::~ChunkMesh()
{
	Clean();
}


void ChunkMesh::Init()
{
	if (vertices.size() != 0) {
		glGenVertexArrays(1, &vao);
		glGenBuffers(1, &vbo);
		glGenBuffers(1, &ib);

		glBindVertexArray(vao);

		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * vertices.size(), (void*)&vertices[0], GL_STREAM_DRAW);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 1, GL_BYTE, GL_FALSE, sizeof(Vertex), (void*)(sizeof(glm::vec3)));
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 1, GL_BYTE, GL_FALSE, sizeof(Vertex), (void*)(sizeof(glm::vec3) + 1));
		glEnableVertexAttribArray(3);
		glVertexAttribPointer(3, 1, GL_BYTE, GL_FALSE, sizeof(Vertex), (void*)(sizeof(glm::vec3) + 2));

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ib);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * indices.size(), (void*)&indices[0], GL_STREAM_DRAW);

		glBindVertexArray(0);
	}
}


void ChunkMesh::Update()
{
	Clean();
	Init();
}


void ChunkMesh::Clean()
{
	glDeleteVertexArrays(1, &vao);

	glDeleteBuffers(1, &vbo);
	glDeleteBuffers(1, &ib);
}


void ChunkMesh::Draw()
{
	if (needsToInit) {
		Update();
		needsToInit = false;
	}

	if (vertices.size() != 0) {
		glBindVertexArray(vao);

		glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);

		glBindVertexArray(0);
	}
}


void ChunkMesh::Clear()
{
	vertices.clear();
	indices.clear();
	infos.clear();
}


void ChunkMesh::AddSurface(const Surface& surf)
{
	for (int i = 0; i < 4; ++i) {
		vertices.push_back(surf.vertices[i]);
	}

	int start = vertices.size() - 4;
	for (int i = 0; i < 6; ++i) {
		indices.push_back(start + surf.indices[i]);
	}
	infos.push_back(surf.info);
	needsToInit = true;
}


int ChunkMesh::FindSurface(const SurfaceInfo& info)
{
	int i = 0;
	while (i < infos.size()) {
		if (infos[i] == info) {
			return i;
		}
		++i;
	}
	return -1;
}


void ChunkMesh::DeleteSurface(int ind)
{
	int vs = vertices.size();
	for (int i = 3; i >= 0; --i) {
		vertices[4 * ind + i] = vertices[vs - 4 + i];
		vertices.pop_back();
	}

	int is = indices.size();
	for (int i = 5; i >= 0; --i) {
		indices[6 * ind + i] = indices[is - 6 + i] - (vs - 4 * ind - 4);
		indices.pop_back();
	}

	infos[ind] = infos.back();
	infos.pop_back();

	needsToInit = true;
}