#pragma once

#include <GL/glew.h>
#include <vector>
#include <glm/glm.hpp>

class ChunkMesh
{
public:

	struct Vertex { // (15 bytes)
		glm::vec3 pos; // (3 * 4 bytes)
		char texx; // (1 byte)
		char texy; // (1 byte)
		char data; // 0-5 norms, 6 water (1 byte)
	};

	struct SurfaceInfo {
		unsigned short block;
		unsigned char side;

		bool operator==(const SurfaceInfo& rhs) {
			return block == rhs.block && side == rhs.side;
		}
	};

	struct Surface {
		Vertex vertices[4];
		int indices[6];
		SurfaceInfo info;
	};

	ChunkMesh();
	~ChunkMesh();

	// For rendering
	void Init();
	void Update();
	void Clean();
	void Draw();
	void Clear();

	// For updating
	void AddSurface(const Surface&);
	int FindSurface(const SurfaceInfo&);
	void DeleteSurface(int);

protected:
	std::vector<Vertex> vertices;
	std::vector<int> indices;
	std::vector<SurfaceInfo> infos;

	GLuint vao;
	GLuint vbo;
	GLuint ib;

	bool needsToInit = true;
};
