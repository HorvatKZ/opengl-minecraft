#version 330 core

// Input from VBO
in vec3 vs_in_pos;
in float vs_in_texx;
in float vs_in_texy;
in float vs_in_norm;

// Output to pipeline
out vec3 vs_out_pos;
out vec3 vs_out_norm;
out vec2 vs_out_tex;
out float vs_out_alpha;

// Uniform variables
uniform mat4 MVP;
uniform mat4 world;
uniform mat4 worldIT;

void main()
{
	gl_Position = MVP * vec4( vs_in_pos, 1 );
	
	vs_out_pos = (world * vec4(vs_in_pos, 1)).xyz;
	vs_out_tex = vec2(vs_in_texx / 16.f, vs_in_texy / 16.f);
	vs_out_alpha = 1.f;

	if (vs_in_norm < 1.f)
	{
		vs_out_norm = vec3(0, 1, 0); // Up
	}
	else if (vs_in_norm < 2.f)
	{
		vs_out_norm = vec3(0, -1, 0); // Down
	}
	else if (vs_in_norm < 3.f)
	{
		vs_out_norm = vec3(-1, 0, 0); // Left
	}
	else if (vs_in_norm < 4.f)
	{
		vs_out_norm = vec3(1, 0, 0); // Right
	}
	else if (vs_in_norm < 5.f)
	{
		vs_out_norm = vec3(0, 0, 1); // Top
	}
	else if (vs_in_norm < 6.f)
	{
		vs_out_norm = vec3(0, 0, -1); // Bottom
	}
	else
	{ // water
		vs_out_norm = vec3(0, 1, 0);
		vs_out_alpha = 0.8f;
	}

	vs_out_norm = (worldIT * vec4(vs_out_norm, 1)).xyz;
}