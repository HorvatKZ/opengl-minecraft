#pragma once

// C++ includes
#include <memory>
#include <vector>
#include <queue>
#include <thread>
#include <mutex>

// GLEW
#include <GL/glew.h>

// SDL
#include <SDL.h>
#include <SDL_opengl.h>

// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform2.hpp>

// OGL Base
#include "includes/gCamera.h"
#include "includes/ProgramObject.h"
#include "includes/BufferObject.h"
#include "includes/VertexArrayObject.h"
#include "includes/TextureObject.h"

// Game
#include "Chunk.h";


class MineCraft
{
public:

	struct GenerationTask
	{
		glm::vec3 pos;
	};

	enum GameMode { SPECTATOR, CREATIVE };
	GameMode gm = CREATIVE;

	MineCraft(void);
	~MineCraft(void);

	bool Init();
	void Clean();

	void Update();
	void Render();

	void KeyboardDown(SDL_KeyboardEvent&);
	void KeyboardUp(SDL_KeyboardEvent&);
	void MouseMove(SDL_MouseMotionEvent&);
	void MouseDown(SDL_MouseButtonEvent&);
	void MouseUp(SDL_MouseButtonEvent&);
	void MouseWheel(SDL_MouseWheelEvent&);
	void Resize(int, int);

protected:
	// programs
	ProgramObject		m_program;
	ProgramObject		m_programSkybox;
	ProgramObject		m_programCrosshair;

	int					viewport_w;
	int					viewport_h;

	VertexArrayObject	m_SkyboxVao;
	IndexBuffer			m_SkyboxIndices;	
	ArrayBuffer			m_SkyboxPos;		

	gCamera				m_camera;
	bool				cursorInvisible = true;

	Texture2D			m_atlasTexture;

	GLuint				m_skyboxTexture;

	// For FPS
	float lastWriteTime = 0;
	int ticks = 0;
	void CountFPS(float);


	// Init functions
	void InitShaders();
	void InitChunks();
	void InitSkyBox();

	// For representing chunks
	std::vector<Chunk*> chunks;
	inline int _indexer(int i, int j) { return ((((i + sv) % N) + N) % N) * N + ((((j + sh) % N) + N) % N); }
	inline int _center() { return _indexer(RENDER_DISTANCE - 1, RENDER_DISTANCE - 1); }
	inline int _posToI(const glm::vec3& pos) { return (int)pos.z / 16 - sv + RENDER_DISTANCE - 1; }
	inline int _posToJ(const glm::vec3& pos) { return (int)pos.x / 16 - sh + RENDER_DISTANCE - 1; }
	int sv = 0, sh = 0; // shift vertical, shift horizontal;

	// For multithreaded chunk generation
	std::thread genThread;
	std::queue<GenerationTask> generationTasks;
	std::mutex lockChunks;
	std::mutex lockGenTasks;
	void ManageGenTasks(const glm::vec3&, const glm::vec3&);	// Updates generation tasks (main thread)
	void Generation();		// Does the generation itself (generation thread)
	bool isEnded = false;

	// Constants
	const int RENDER_DISTANCE = 16; // Should not be lower than 3
	const int N = 2 * RENDER_DISTANCE - 1;
	const int ACTION_DISTANCE = 7;

	// For block placing
	unsigned char selectedBlock = Chunk::STONE;

	// Optimisation
	inline bool isInView(const glm::vec2&);
};

