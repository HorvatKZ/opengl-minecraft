#version 330 core

// Input from pipeline
in vec3 vs_out_pos;

// Output
out vec4 fs_out_col;

// Sampler
uniform samplerCube skyboxTexture;

void main()
{
	fs_out_col = texture( skyboxTexture, (vs_out_pos) );
}