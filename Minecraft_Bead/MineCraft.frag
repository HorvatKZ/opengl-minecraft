#version 330 core

// Input from VBO
in vec3 vs_out_pos;
in vec3 vs_out_norm;
in vec2 vs_out_tex;
in float vs_out_alpha;

// Output
out vec4 fs_out_col;

// For light
uniform vec3 light_dir = vec3(-3, -6, -1);
uniform vec3 La = vec3(0.4, 0.4, 0.4);
uniform vec3 Ld = vec3(0.5, 0.5, 0.5);

// Texture atlas
uniform sampler2D atlas;

void main()
{
	vec3 ambient = La;

	vec3 normal = normalize(vs_out_norm);
	vec3 to_light = normalize(-light_dir);
	float cosa = clamp(dot(normal, to_light), 0, 1);
	vec3 diffuse = cosa*Ld;
	
	fs_out_col = vec4(ambient + diffuse, vs_out_alpha) * texture(atlas, vs_out_tex);
}